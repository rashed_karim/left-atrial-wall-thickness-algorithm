/****************************************************************
*      Author: 
*      Dr. Rashed Karim 
*      Department of Biomedical Engineering, King's College London
*      Email: rashed 'dot' karim @kcl.ac.uk  
*      Copyright (c) 2016
*****************************************************************/

// Makes use of the proprietory Image Registration Toolkit library (IRTK) from Imperial Colllege London. 
#include "mirtk/Common.h"
#include "mirtk/Options.h"
#include "mirtk/IOConfig.h"
#include "mirtk/GenericImage.h"

#include "mirtk/EuclideanDistanceTransform.h"

// Other standard C libraries 
#include <fstream>
#include <vector>

using namespace mirtk;


int main(int argc, char **argv)
{
    double x_pixel_size, y_pixel_size, z_pixel_size; 
    char *wall_in, *edt_in, *output_fn;
    double t1, t2, iterations, thickness;
    bool skip_neigbours; 
    
    int maxX, maxY, maxZ, maxX2, maxY2, maxZ2;
    int is_debug_mode;  
    
    RealPixel *edt_p;
    GreyPixel *wall_p;
    
    RealImage edt_img;
    GreyImage wall_img;
    
    short*** out_img; 
    
    if (argc != 4) 
    {
        cout << "\nUsage: comp_wall <concat_post> <edt_post> <output_txt_post>\n\nThis program outputs" 
        "wall thickness to a text file" << endl;
        exit(1); 
    }

    // get input parameters 
    
    wall_in = argv[1]; 
    argv++; 
    argc--; 
    edt_in = argv[1]; 
    argv++; 
    argc--; 
    output_fn = argv[1];  
    argv++; 
    argc--; 
    
    wall_img.Read(wall_in);
    edt_img.Read(edt_in);
    
    maxX = wall_img.GetX(); 
    maxY = wall_img.GetY(); 
    maxZ = wall_img.GetZ(); 
    
    ofstream out; 
    out.open(output_fn);
    
    // initialize output image
    int i,j,k,a,b,c; 
    
    
    
    cout << "Assuming isotropic or nearly isotropic image .." << endl;
    
    cout << "\nX size = " << wall_img.GetXSize() << "\nY size= " << wall_img.GetYSize() << "\nZ = " << wall_img.GetZSize() << endl;
    cout << "\nTaking pixel size in X as image resolution.. " << endl;
    
    x_pixel_size = wall_img.GetXSize();
       
       
    // Looking for pixels that have label 2 and has a background as neighbour 
    // These are pixels that are wall pixels and outermost (from LA chamber)   
    for (i=0;i<maxX;i++)
    {
        for (j=0;j<maxY;j++)
        {
            for (k=0;k<maxZ;k++)
            {
                // iterate through neighbours 
                skip_neigbours = false; 
                for (a=-1;a<1 && !skip_neigbours;a++)
                {
                    for (b=-1;b<1 && !skip_neigbours;b++)
                    {
                        for (c=-1;c<1 && !skip_neigbours;c++)
                        {
                            if (i+a >= 0 && i+a < maxX && j+b >= 0 && j+b < maxY && k+c >= 0 && k+c < maxZ)
                            {
                                short neighbour_pixel = wall_img.Get(i+a, j+b, k+c);
                                short this_pixel = wall_img.Get(i,j,k);
                                
                                if (this_pixel == 2 && neighbour_pixel == 0) {
                                     
                                     // found pixel that is a wall pixel 
                                     skip_neigbours = true;
                                     
                                     thickness = edt_img.Get(i,j,k);
                                     
                                     // Euclidean distance is squared
                                     thickness = sqrt(thickness)*x_pixel_size;
                                     
                                     // Ignoring 0 thickness 
                                     if (thickness > 0)
                                        out << std::setprecision(2) << std::fixed << thickness << endl;  
                                   
                                }
                            }       // end if
                        }
                    }
                } // end searching immediate neighbours  
            }
        }
    }   // end image iteration loop 
    
   
    out.close();
      
    
}
