# Atrial wall thickness #

This program was used to analyse the atrial wall thickness in CT segmentations from the [MICCAI STACOM SLAWT 2016 segmentation challenge ](https://www.doc.ic.ac.uk/~rkarim/la_lv_framework/wall/) 

The program uses outputs from two other programs: 

1. [A simple mask concatenator](https://bitbucket.org/rashed_karim/concat)
2. [Euclidean distance transform computations](https://bitbucket.org/rashed_karim/edt)

In the rest of this documentation, outputs from the above programs will be referred to as **concat** and **edt**. 

### Usage ###

The usage is as follows. Note that the wall is analysed in sections, posterior and anterior sections separately: 


```
#!python

comp_wall CTxx_concat_post.nii CTxx_edt_post.nii CTxx_post.txt 
```

### Contacts ###

* Rashed Karim (rashed.karim@kcl.ac.uk)